# -*- coding: utf-8 -*-
'''
Created on 2013. 6. 13.

@author: KIm Wansu
'''
from django.contrib import admin
from happyzehn.goods.models import Category
from happyzehn.goods.models import Manufacturer
from happyzehn.goods.models import Goods
from happyzehn.goods.models import KeySwitch
from happyzehn.goods.models import KbdLayout
from happyzehn.goods.models import KbdGoods
from happyzehn.goods.models import AccCategory
from happyzehn.goods.models import AccGoods
from happyzehn.goods.models import Review
from happyzehn.goods.models import GoodsInquire

admin.site.register(Category)
admin.site.register(Goods)
admin.site.register(Manufacturer)
admin.site.register(KeySwitch)
admin.site.register(KbdLayout)
admin.site.register(KbdGoods)
admin.site.register(AccCategory)
admin.site.register(AccGoods)
admin.site.register(Review)
admin.site.register(GoodsInquire)
