# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.core.validators import MinValueValidator, MaxValueValidator


class Category(models.Model):
    name = models.CharField(max_length=10, unique=True,
                            null=False, blank=False)

    def __unicode__(self):
        return self.name


class Manufacturer(models.Model):
    name = models.CharField(max_length=16, unique=True,
                            null=False, blank=False)

    def __unicode__(self):
        return self.name


class KeySwitch(models.Model):
    name = models.CharField(max_length=16, unique=True,
                            null=False, blank=False)

    def __unicode__(self):
        return self.name


class KbdLayout(models.Model):
    name = models.CharField(max_length=16, unique=True,
                            null=False, blank=False)

    def __unicode__(self):
        return self.name


class AccCategory(models.Model):
    name = models.CharField(max_length=16, unique=True,
                            null=False, blank=False)

    def __unicode__(self):
        return self.name


class Goods(models.Model):
    name = models.CharField(max_length=72, unique=True,
                            null=False, blank=False)
    manufacturer = models.ForeignKey(Manufacturer)
    category = models.ForeignKey(Category)
    thumbnail = models.ImageField(upload_to='goods_tb')
    desc = models.TextField()
    descimg = models.ImageField(upload_to='goods_img')
    regidate = models.DateTimeField(auto_now_add=True)
    stock = models.PositiveIntegerField(null=False)
    price = models.PositiveIntegerField(null=False)
    mileage = models.PositiveIntegerField(null=False,
                                          validators=[MaxValueValidator(price),
                                                      ])
    is_sale = models.BooleanField(null=False, default=True)
    sales = models.PositiveIntegerField(null=False, default=0)
    avg_score = models.PositiveIntegerField(null=False, default=0)

    def __unicode__(self):
        return self.name


class KbdGoods(Goods):
    switch = models.ForeignKey(KeySwitch)
    layout = models.ForeignKey(KbdLayout)
    
    def __unicode__(self):
        return self.name


class AccGoods(Goods):
    subcategory = models.ForeignKey(AccCategory)
    
    def __unicode__(self):
        return self.name


class Review(models.Model):
    goods = models.ForeignKey(Goods, null=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=False)
    score = models.PositiveSmallIntegerField(null=False,
                                             default=5,
                                             validators=[MaxValueValidator(10),
                                                         MinValueValidator(0),
                                                         ],
                                             verbose_name='점수')
    content = models.TextField(max_length=2000, null=False, blank=False,
                               verbose_name='내용')
    created = models.DateTimeField(auto_now_add=True, auto_now=True)

    def __unicode__(self):
        return unicode(self.goods) + ' ' + unicode(self.user)


class GoodsInquire(models.Model):
    goods = models.ForeignKey(Goods, null=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=False)
    content = models.TextField(max_length=2000, null=False, blank=False,
                               verbose_name='내용')
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return unicode(self.goods) + ' ' + unicode(self.user)


class GoodsInquire_Answer(models.Model):
    inquire = models.OneToOneField(GoodsInquire)
    answer = models.TextField(max_length=2000, null=True, blank=False,
                              verbose_name='답변')
    answered = models.DateTimeField(auto_now_add=True, null=True)

    def __unicode__(self):
        return unicode(self.inquire)
