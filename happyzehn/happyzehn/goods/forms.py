# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
from django.forms.widgets import CheckboxSelectMultiple
from happyzehn.goods.models import *


SORT_BY = (
    (0, '최신상품순'),
    (1, '저가격순'),
    (2, '판매량순'),
    (3, '평점순'),
)


# 키보드 상품 검색 폼
class KbdFilterForm(forms.Form):
    manufacturer = forms.ModelMultipleChoiceField(queryset=(Manufacturer
                                                            .objects.all()),
                                                  label='제조사',
                                                  initial=(Manufacturer
                                                           .objects.all()),
                                                  widget=CheckboxSelectMultiple)
    switch = forms.ModelMultipleChoiceField(queryset=KeySwitch.objects.all(),
                                            label='키 스위치',
                                            initial=KeySwitch.objects.all(),
                                            widget=CheckboxSelectMultiple)
    layout = forms.ModelMultipleChoiceField(queryset=KbdLayout.objects.all(),
                                            label='레이아웃',
                                            initial=KbdLayout.objects.all(),
                                            widget=CheckboxSelectMultiple)
    
    sort_by = forms.ChoiceField(choices=SORT_BY)


# 악세사리 상품 검색 폼
class AccFilterForm(forms.Form):
    manufacturer = forms.ModelMultipleChoiceField(queryset=(Manufacturer
                                                            .objects.all()),
                                                  label='제조사',
                                                  initial=(Manufacturer
                                                           .objects.all()),
                                                  widget=CheckboxSelectMultiple)
    acccategory = forms.ModelMultipleChoiceField(queryset=(AccCategory
                                                           .objects.all()),
                                                 label='소분류',
                                                 initial=(AccCategory
                                                          .objects.all()),
                                                 widget=CheckboxSelectMultiple)
    sort_by = forms.ChoiceField(choices=SORT_BY)


# 상품평 작성 폼
class ReviewForm(ModelForm):
    class Meta:
        model = Review
        exclude = ('goods', 'user', 'created',)


# 상품 문의 작성 폼
class GoodsInquireForm(ModelForm):
    class Meta:
        model = GoodsInquire
        exclude = ('goods', 'user', 'created',)


# 상품 문의 답변 작성 폼
class GoodsInquire_AnswerForm(ModelForm):
    class Meta:
        model = GoodsInquire_Answer
        exclude = ('inquire', 'answered',)
