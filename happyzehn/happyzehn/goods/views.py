# -*- coding: utf-8 -*-
from django.http import HttpResponse

from happyzehn.goods.models import *
from happyzehn.goods.forms import *
from django.shortcuts import render, redirect

ALL = 0
KEYBOARDS = 1
ACCESSORYS = 2
ORDERMADE = 3

REGIDATE_ASC = 1
REGIDATE_DESC = 2
PRICE_ASC = 3
PRICE_DESC = 4
SALES_ASC = 5
SALES_DESC = 6
SCORE_ASC = 7
SCORE_DESC = 8


# 상품 목록
def view_goodslist(request, category=ALL, page=1):
    category = int(category)
    page = int(page)
    per_page = 5
    start_pos = (page-1) * per_page
    end_pos = start_pos + per_page
    criteria_list = ['-regidate', 'price', '-sales', '-score',]

    try:
        if category == ALL:
            goods_list = Goods.objects.all()
            category_name = '전체'
        else:
            goods_list = Goods.objects.filter(category=category)
            category_name = Category.objects.get(id=category).name

    except:
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '잘못된 상품 목록 요청입니다.',
                       'redirect_to': 'default',
                       })

    page_title = 'happyzehn : ' + category_name + ' 상품 목록'

    if request.method == 'POST':
        if category == KEYBOARDS:
            form = KbdFilterForm(request.POST)
            manufacturers = request.POST.getlist('manufacturer')
            switches = request.POST.getlist('switch')
            layouts = request.POST.getlist('layout')
            
            goods_list = (KbdGoods.objects
                          .filter(manufacturer__in=manufacturers)
                          .filter(switch__in=switches)
                          .filter(layout__in=layouts))
    
        elif category == ACCESSORYS:
            form = AccFilterForm(request.POST)
            manufacturers = request.POST.getlist('manufacturer')
            subcategories = request.POST.getlist('acccategory')
    
            goods_list = (AccGoods.objects.all()
                          .filter(manufacturer__in=manufacturers)
                          .filter(subcategory__in=subcategories))
    
        criteria = int(request.POST['sort_by'])
        goods_list = goods_list.order_by(criteria_list[criteria])
        goods_count = goods_list.count()
        page_title = category_name + ' 검색 결과'
        page_count = 1
    
    else:
        if category == KEYBOARDS:
            form = KbdFilterForm()
        elif category == ACCESSORYS:
            form = AccFilterForm()
        else:
            form = None
        criteria = 0
        page_title = category_name + ' 상품 목록'
        goods_count = goods_list.count()
        page_count = goods_count / per_page
        if goods_count%per_page != 0:
            page_count += 1
        
        goods_list = goods_list[start_pos:end_pos]

    variables = {'page_title': page_title,
                 'location': page_title,
                 'form': form,
                 'category_name': category_name,
                 'goods_list': goods_list,
                 'goods_count': goods_count,
                 'page': page,
                 'page_count': page_count,
                 'page_range': range(1, page_count+1),
                 }

    if category == KEYBOARDS:
        variables['form'] = KbdFilterForm()
    elif category == ACCESSORYS:
        variables['form'] = AccFilterForm()

    return render(request, 'goods/goods_list.html', variables)


# 상품 정보 보기
def view_goods(request, goods_id):
    goods_id = int(goods_id)

    try:
        goods_info = Goods.objects.get(id=goods_id)
        category = goods_info.category.pk
        if category == KEYBOARDS:
            goods_info = KbdGoods.objects.get(id=goods_id)
        elif category == ACCESSORYS:
            goods_info = AccGoods.objects.get(id=goods_id)
    except:
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '존재하지 않는 상품입니다.',
                       'redirect_to': 'default',
                       })

    page_title = 'happyzehn : ' + goods_info.name + ' 상품 정보'

    reviews = Review.objects.filter(goods=goods_id)
    inquires = GoodsInquire.objects.filter(goods=goods_id)
    inquire_answers = GoodsInquire_Answer.objects.filter(inquire__in=inquires)

    return render(request, 'goods/goods_info.html',
                  {'page_title': page_title,
                   'location': (goods_info.name + ' 상품 정보'),
                   'goods_info': goods_info,
                   'review_list': reviews,
                   'inquire_list': inquires,
                   'answer_list': inquire_answers,
                   })


# 상품평 작성
def view_write_review(request, goods_id):
    if not request.user.is_authenticated():
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '잘못된 접근입니다',
                       'redirect_to': 'default',
                       })

    if request.method == 'POST':
        goods = Goods.objects.get(id=goods_id)
        new_review = Review(goods=goods, user=request.user)
        form = ReviewForm(request.POST, instance=new_review)
        if form.is_valid():
            form.save()

            review_list = Review.objects.filter(goods=goods_id)
            total = 0
            for r in review_list:
                total += r.score

            goods = Goods.objects.get(id=goods_id)
            goods.avg_score = round(float(total) / review_list.count(), 2)
            goods.save()
            return redirect(view_goods, goods_id)

    else:
        form = ReviewForm()

    goods_name = Goods.objects.get(id=goods_id).name
    page_title = 'happyzehn : ' + goods_name + ' 상품평 작성'
    return render(request, 'goods/form.html',
                  {'page_title': page_title,
                   'location': (goods_name + ' 상품평 작성'),
                   'tid': goods_id,
                   'form':form,
                   })


# 상품 문의하기
def view_write_inquire(request, goods_id):
    if not request.user.is_authenticated():
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '잘못된 접근입니다',
                       'redirect_to': 'default'
                      })

    if request.method == 'POST':
        goods = Goods.objects.get(id=goods_id)
        new_inquire = GoodsInquire(goods=goods, user=request.user)
        form = GoodsInquireForm(request.POST, instance=new_inquire)
        if form.is_valid():
            form.save()

            return redirect(view_goods, goods_id)

    else:
        form = GoodsInquireForm()

    goods_name = Goods.objects.get(id=goods_id).name
    page_title = 'happyzehn : ' + goods_name + ' 상품 문의 작성'
    return render(request, 'goods/form.html',
                  {'page_title': page_title,
                   'location': (goods_name + ' 상품 문의 작성'),
                   'tid': goods_id,
                   'form': form,
                  })


# 상품 문의 답변하기(관리자)
def view_answer_inquire(request, inquire_id):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })

    inquire = GoodsInquire.objects.get(id=inquire_id)
    goods_id = inquire.goods.id

    if request.method == 'POST':
        new_answer = GoodsInquire_Answer(inquire=inquire)
        form = GoodsInquire_AnswerForm(request.POST, instance=new_answer)
        if form.is_valid():
            form.save()

            return redirect(view_goods, goods_id)

    else:
        form = GoodsInquire_AnswerForm()

    goods_name = inquire.goods.name
    page_title = 'happyzehn : ' + goods_name + ' 상품 문의 답변 작성'
    return render(request, 'goods/form.html',
                  {'page_title': page_title,
                   'location': (goods_name + ' 상품 문의 답변 작성'),
                   'tid': inquire_id,
                   'form': form,
                   })
