# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.utils.timezone import utc
from happyzehn.goods.models import Goods
from happyzehn.member.models import UserProfile, UserProfile_Recipient
from happyzehn.order.cart import Cart
from happyzehn.order.models import Order, PaymentMethod, Item
import datetime
import calendar
from django.contrib.auth.models import User


# 장바구니에 상품 추가
def add_to_cart(request, product_id, quantity):
    quantity = int(quantity)
    if not request.user.is_authenticated():
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })
    
    product = Goods.objects.get(id=product_id)
    if product.stock < quantity:
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '재고 수량보다 많이 주문할 수 없습니다.',
                       'redirect_to': 'goods_info',
                       'arg': product_id,
                       })
    
    cart = Cart(request)
    cart.add(product, product.price, quantity)

    return redirect(view_cart)


# 장바구니에서 상품 삭제
def remove_from_cart(request, product_id):
    if not request.user.is_authenticated():
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })

    product = Goods.objects.get(id=product_id)
    cart = Cart(request)
    cart.remove(product)

    return redirect(view_cart)


# 장바구니의 상품 수량 변경
def update_cart_product_quantity(request):
    if not request.user.is_authenticated():
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })

    product_id = int(request.POST['product_id'])
    quantity = int(request.POST['quantity'])
    product = Goods.objects.get(id=product_id)
    if product.stock < quantity:
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '재고 수량보다 많이 주문할 수 없습니다.',
                       'redirect_to': 'goods_info',
                       'arg': product_id,
                       })
    cart = Cart(request)
    cart.update(product, quantity)

    return redirect(view_cart)


# 장바구니 비우기
def clean_cart(request):
    if not request.user.is_authenticated():
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })

    Cart(request).clear()
    return redirect(view_cart)


# 장바구니
def view_cart(request):
    if not request.user.is_authenticated():
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })

    page_title = 'happyzehn : 장바구니'
    variables = {'page_title': page_title,
                 'location': '장바구니',
                 }
    variables.update(dict(cart=Cart(request)))
    return render(request, 'order/cart.html', variables)


# 배송지 선택
def view_choice_recipient(request):
    cart = Cart(request)
    if not request.user.is_authenticated() or cart.count() == 0:
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })

    if request.method == 'POST':
        rcpt_id = int(request.POST['rcpt_id'])
        recipient = UserProfile_Recipient.objects.get(pk=rcpt_id)
        cart.cart.checked_out = True
        
        try:
            order = Order(user=request.user,
                          cart=cart.cart,
                          delivery=recipient,
                          order_date=datetime.datetime.now(),
                          total_price=cart.summary(),
                          payment=cart.summary(),
                          )
        except:
            return redirect(view_cart)
        order.save()
        return redirect(view_choice_payment)

    page_title = 'happyzehn : 배송지 선택'
    recipient = UserProfile_Recipient.objects.filter(user=request.user)
    return render(request, 'order/choice_recipient.html',
                  {'page_title': page_title,
                   'location': '배송지 선택',
                   'recipient': recipient,
                   })


# 결제 수단 선택
def view_choice_payment(request):
    cart = Cart(request)
    if (not request.user.is_authenticated() or
        not Order.objects.get(cart=cart.cart)):
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })

    if request.method == 'POST':
        paid_point = int(request.POST['paid_point'])
        paid_method = int(request.POST['paid_method'])
        user_profile = UserProfile.objects.get(user=request.user)
        if paid_point <= user_profile.mileage:
            order = Order.objects.get(cart=cart.cart)
            order.total_price = cart.summary()
            order.payment_method = PaymentMethod.objects.get(pk=paid_method)
            order.payment = cart.summary() - paid_point
            order.save()
            user_profile.mileage -= paid_point
            user_profile.save()
            return redirect(view_input_payment_info)
        paid_point_error = True
    else:
        paid_point_error = False

    page_title = 'happyzehn : 결제 수단 선택'
    paid_methods = PaymentMethod.objects.all()
    user_profile = UserProfile.objects.get(user=request.user)
    order = Order.objects.get(cart=cart.cart)
    return render(request, 'order/choice_paid_method.html',
                  {'page_title': page_title,
                   'location': '결제 수단 선택',
                   'mileage': user_profile.mileage,
                   'price': order.payment,
                   'paid_methods': paid_methods,
                   'paid_point_error': paid_point_error,
                   })


# 결제 정보 입력
def view_input_payment_info(request):
    # 결제 정보 확인은 신용카드 유효기간만 하고 나머지는 더미
    CREDIT_CARD = 1
    BANK_TRANSFER = 2

    cart = Cart(request)
    if (not request.user.is_authenticated() or
        not Order.objects.get(cart=cart.cart)):
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })

    if request.method == 'POST':
        paid_method = int(request.POST['paid_method'])
        if paid_method == CREDIT_CARD:
            card_no = request.POST['card_no']
            card_pw = request.POST['card_pw']
            card_year = int(request.POST['card_year'])
            card_month = int(request.POST['card_month'])
            card_cvc = request.POST['card_cvc']

            card_valid = datetime.date(card_year, card_month,
                                       calendar.monthrange(card_year,
                                                           card_month)[-1])
            diff = card_valid - datetime.date.today()
            if diff.days > 0:
                return redirect(view_payment)

            error_msg = '신용카드 유효 기간이 끝났습니다.'

        elif paid_method == BANK_TRANSFER:
            account_no = request.POST['acc_no']
            account_pw = request.POST['acc_pw']
            account_name = request.POST['acc_name']
            return redirect(view_payment)
    else:
        error_msg = None

    page_title = 'happyzehn : 결제 정보 입력'
    order = Order.objects.get(cart=cart.cart)
    paid_method = order.payment_method.id
    return render(request, 'order/input_paid_info.html',
                  {'page_title': page_title,
                   'location': '결제 정보 입력',
                   'paid_method': paid_method,
                   'error_msg': error_msg,
                   })


# 결제하기
def view_payment(request):
    # 결제가 되었으면 배송된 것으로 처리
    cart = Cart(request)
    if (not request.user.is_authenticated() or
        not Order.objects.get(cart=cart.cart)):
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })

    order = Order.objects.get(cart=cart.cart)
    order.paymented = True
    order.payment_date = datetime.datetime.now()
    order.shipped = True
    order.shipped_date = datetime.datetime.now()
    order.save()
    
    order_items = Item.objects.filter(cart=cart.cart)
    total_price = 0
    total_mileage = 0
    for item in order_items:
        product = Goods.objects.get(pk=item.product.id)
        product.stock -= item.quantity
        product.sales += item.quantity
        total_mileage += product.mileage * item.quantity
        total_price += product.price * item.quantity
        product.save()

    page_title = 'happyzehn : 결제 완료'
    return render(request, 'order/order_complete.html',
                   {'page_title': page_title,
                    'location': '결제 완료',
                    'order_items': order_items,
                    'total_price': total_price,
                    'total_mileage': total_mileage,
                    })


# 주문 현황
def view_order(request):
    if not request.user.is_authenticated():
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })

    order_list = Order.objects.filter(user=request.user)

    page_title = 'happyzehn : 주문 현황'
    return render(request, 'order/order_list.html',
                  {'page_title': page_title,
                   'location': '주문 현황',
                   'order_list': order_list,
                   })

# 주문 중단
def view_stop_order(request):
    if not request.user.is_authenticated():
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })
    
    cart = Cart(request)
    order = Order.objects.get(cart=cart.cart)
    if order:
        order.delete()
        cart.clear()
    
    return redirect('/')

# 주문 취소/환불
def view_cancel(request):
    if (not request.user.is_authenticated() or
        request.method != 'POST'):
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })

    else:
        order_id = int(request.POST['order_id'])
        order = Order.objects.get(id=order_id)
        order.canceled = True
        order.save()
        if order.total_price != order.payment:
            user_profile = UserProfile.objects.get(user=request.user)
            user_profile.mileage += (order.total_price - order.payment)
            user_profile.save()

        order_items = Item.objects.filter(cart=order.cart)
        for item in order_items:
            product = Goods.objects.get(pk=item.product.id)
            product.stock += item.quantity
            product.save()

        if order.shipped == 'True':
            method = '환불'
        else:
            method = '주문 취소'

        msg = method + ' 처리가 완료되었습니다.'
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': (method + ' 완료'),
                       'type': 'success',
                       'msg': msg,
                       'redirect_to': 'default'
                       })


# 주문 결정
def view_confirm_order(request):
    if (not request.user.is_authenticated() or
        request.method != 'POST'):
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '오류',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })

    else:
        order_id = int(request.POST['order_id'])
        order = Order.objects.get(id=order_id)
        order.checked_out =  True
        order.save()
        
        cart = order.cart
        order_items = Item.objects.filter(cart=cart)
        total_mileage = 0
        for item in order_items:
            product = Goods.objects.get(pk=item.product.id)
            total_mileage += product.mileage * item.quantity
        user = UserProfile.objects.get(user=request.user)
        user.mileage += total_mileage
        user.save()

    msg = ('주문 결정 처리가 완료되었습니다. 저희 상품을 구매해 주셔서 ' +
           '감사합니다.')
    return render(request, 'result.html',
                  {'page_title': '요청 결과',
                   'location': '주문 결정 완료',
                   'type': 'success',
                   'msg': msg,
                   'redirect_to': 'default',
                  })