# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.contenttypes.models import ContentType
from happyzehn.member.models import UserProfile_Recipient, User
from happyzehn.goods.models import Goods


class Cart(models.Model):
    owner = models.ForeignKey(User, null=False)
    creation_date = models.DateTimeField()
    checked_out = models.BooleanField(default=False)

    class Meta:
        ordering=('-creation_date',)

    def __unicode__(self):
        return unicode(self.creation_date)

class ItemManager(models.Manager):
    def get(self, *args, **kwargs):
        if 'product' in kwargs:
            kwargs['content_type'] = (ContentType.objects
                                      .get_for_model(type(kwargs['product'])))
            kwargs['object_id'] = kwargs['product'].pk
            del(kwargs['product'])
        return super(ItemManager, self).get(*args, **kwargs)


class Item(models.Model):
    cart = models.ForeignKey(Cart)
    quantity = models.PositiveIntegerField()
    unit_price = models.IntegerField()
    
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    
    objects = ItemManager()
    
    class Meta:
        ordering = ('cart', )

    def __unicode__(self):
        return 'Item in cart: ' + unicode(self.cart)

    def total_price(self):
        return self.quantity * self.unit_price
    total_price = property(total_price)
    
    def get_product(self):
        return self.content_type.get_object_for_this_type(id=self.object_id)
    
    def set_product(self, product):
        self.content_type = ContentType.objects.get_for_model(type(product))
        self.object_id = product.pk
    
    product = property(get_product, set_product)


class PaymentMethod(models.Model):
    name = models.CharField(max_length=10, null=False, blank=False)

    def __unicode__(self):
        return self.name


class Order(models.Model):
    user = models.ForeignKey(User, null=False)
    cart = models.ForeignKey(Cart, unique=True, null=False)
    delivery = models.ForeignKey(UserProfile_Recipient)
    order_date = models.DateTimeField()
    total_price = models.IntegerField()
    payment = models.IntegerField()
    paymented = models.BooleanField(default=False)
    payment_method = models.ForeignKey(PaymentMethod, null=True)
    payment_date = models.DateTimeField(null=True)
    shipped = models.BooleanField(default=False)
    shipped_date = models.DateTimeField(null=True)
    canceled = models.BooleanField(default=False)
    checked_out = models.BooleanField(default=False)

    def __unicode__(self):
        return self.pk