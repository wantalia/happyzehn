# -*- coding: utf-8 -*-
'''
Created on 2013. 6. 13.

@author: KIm Wansu
'''
from django.contrib import admin
from happyzehn.order.models import Cart
from happyzehn.order.models import Item
from happyzehn.order.models import PaymentMethod
from happyzehn.order.models import Order

admin.site.register(Cart)
admin.site.register(Item)
admin.site.register(PaymentMethod)
admin.site.register(Order)
