# -*- coding: utf-8 -*-

'''
메모

공통: 로그인, 회원가입
회원공통: 고객센터, 장바구니
관리자: 관리자화면

처음-->상품 목록
상품 목록-->상품정보, 장바구니에 담기, 검색(같은화면)
상품정보-->장바구니에 담기, 바로 구매하기, 상품평남기기/수정·삭제, 문의하기/수정·삭제
장바구니-->구매하기
상품평남기기-->상품정보
문의하기-->상품정보
※ 문의는 답변이 달리기 전까지만 수정 가능

회원가입
--------
정보입력-->처음

구매하기
--------
포인트 차감, 결제수단 선택-->결제정보입력-->결제과정(생략)-->결제완료
※신용카드 결제는 카드번호, 유효기간, CVC입력하고 유효기간만 확인함(나머지는 더미)
※무통입금은 은행선택하고 입금자 입력만 함(결제를 누르면 입금한 것으로 처리)



고객센터
--------
회원정보수정
1:1문의
주문현황-->진행중인 주문과 완료된 주문으로 구분
FAQ


관리자화면(별도작성만)
----------
상품 등록
상품 정보 수정/삭제



※ 잘못된 접근: 상품번호 없음, 권한 없음 등 --> 처음 페이지로 이동, 메시지 표시
※ 사소한 오류(단순 입력 누락 등) --> 제자리로 이동, 메시지 표시

카테고리
--------
1: 키보드
2: 악세사리/부품
3: 특수주문(조립, 각인 등)
'''