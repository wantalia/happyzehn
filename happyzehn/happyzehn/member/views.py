# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from happyzehn.goods.models import Goods
from happyzehn.member.forms import *
from happyzehn.member.models import *


# 처음
def view_home(request):
    page_title = 'happyzehn : 열 손가락 전부 편안한 키보드 전문 쇼핑몰'
    goods_list = Goods.objects.all()

    new_goods = goods_list.order_by('-regidate')[:3]
    popular_goods = goods_list.order_by('-sales')[:3]
    rated_goods = goods_list.order_by('-avg_score')[:3]

    return render(request, 'default.html',
                  {'page_title': page_title,
                   'location': '초기 화면',
                   'new_goods': new_goods,
                   'popular_goods': popular_goods,
                   'rated_goods': rated_goods,
                   })


# 로그인
def view_login(request):
    if request.user.is_authenticated():
        return redirect(view_home)

    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect(view_home)
                else:
                    msg = '비활성 사용자입니다. 관리자에게 문의하십시오.'
                    return render(request, 'result.html',
                                  {'page_title': '요청 결과',
                                   'location': '에러',
                                   'type': 'error',
                                   'msg': msg,
                                   'redirect_to': 'default',
                                   })
            else:
                return render(request, 'result.html',
                              {'page_title': '요청 결과',
                               'location': '에러',
                               'type': 'error',
                               'msg': '로그인에 실패했습니다.',
                               'redirect_to': 'login',})
    else:
        page_title = 'happyzehn : 로그인'
        form = LoginForm()
        return render(request, 'form.html',
                      {'page_title': page_title,
                       'location': '로그인',
                       'form': form,
                       })


# 로그아웃
def view_logout(request):
    if not request.user.is_authenticated():
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '에러',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })

    logout(request)
    return HttpResponseRedirect('/')


# 회원 가입
def view_signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            firstname = form.cleaned_data['firstname']
            lastname = form.cleaned_data['lastname']
            fullname = (form.cleaned_data['lastname'] + ' ' +
                        form.cleaned_data['firstname'])
            password = form.cleaned_data['password1']
            email = form.cleaned_data['email']

            address = form.cleaned_data['address']
            phone = form.cleaned_data['phone']

            user = User.objects.create_user(username=username,
                                            first_name=firstname,
                                            last_name=lastname,
                                            password=password,
                                            email=email,
                                            )
            UserProfile.objects.create(user=user,
                                       address=address,
                                       phone=phone,
                                       mileage=0,)

            UserProfile_Recipient.objects.create(user=user,
	                                         name='기본',
                                                 rcpt_name=fullname,
                                                 rcpt_addr=address,
                                                 rcpt_phone=phone,)

            return render(request, 'result.html',
                          {'page_title': '요청 결과',
                           'location': '회원 가입 성공',
                           'type': 'success',
                           'msg': '회원 가입을 축하합니다.',
                           'redirect_to': 'default',
                           })
    else:
        form = SignUpForm()

    page_title = 'happyzehn : 회원 가입'
    return render(request, 'form.html',
                  {'page_title': page_title,
                   'location': '회원 가입',
                   'form': form,
                   })


# 암호 변경
def view_passwd(request):
    if not request.user.is_authenticated():
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '에러',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })

    if request.method == 'POST':
        form = PasswdForm(request.POST)
        if form.is_valid():
            current_user = User.objects.get(pk=request.user.id)
            current_user.set_password(request.POST['password2'])
	    current_user.save()
            return render(request, 'result.html',
                          {'page_title': '요청 결과',
                           'location': '성공',
                           'type': 'success',
                           'msg': '암호 변경을 성공하였습니다.',
                           'redirect_to': 'default',
                           })

    else:
        form = PasswdForm()

    page_title = 'happyzehn : 암호 변경'
    return render(request, 'form.html',
                  {'page_title': page_title,
                   'location': '암호 변경',
                   'form': form,
                  })


# 회원 정보 수정
def view_update(request):
    if not request.user.is_authenticated():
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '에러',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })

    if request.method == 'POST':
        form = UpdateForm(request.POST)
        if form.is_valid():
            user = User.objects.get(pk=request.user.id)
            user_profile = UserProfile.objects.get(user=user.id)

            user.email = form.cleaned_data['email']
            user_profile.address = form.cleaned_data['address']
            user_profile.phone = form.cleaned_data['phone']

            user.save()
            user_profile.save()

            return render(request, 'result.html',
                          {'page_title': '요청 결과',
                           'location': '회원 정보 수정 성공',
                           'type': 'success',
                           'msg': '회원 정보가 수정되었습니다.',
                           'redirect_to': 'default',
                           })

    page_title = 'happyzehn : 회원 정보 수정'
    form = UpdateForm()
    return render(request, 'form.html',
                  {'page_title': page_title,
                   'location': '회원 정보 수정',
                   'form': form,
                   })


# 고객 센터
def view_cscenter(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/?next=%s' % request.path)

    page_title = 'happyzehn : 고객센터'
    return render(request, 'member/cscenter.html',
                  {'page_title': page_title,
                   'location': '고객센터',
                   })


# 배송지 목록
def view_recipient_list(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/?next=%s' % request.path)

    page_title = 'happyzehn : 배송지 목록'

    if request.method == 'POST':
        rcpt_id = request.POST['rcpt_id']
        rcpt_item = UserProfile_Recipient.objects.get(pk=rcpt_id)
        rcpt_item.delete()
        rcpt_list = UserProfile_Recipient.objects.filter(user=request.user.id)
        msg = '배송지 삭제가 완료되었습니다.'
        return render(request, 'member/rcpt_list.html',
                      {'page_title':page_title,
                       'location': '배송지 목록',
                       'rcpt_list': rcpt_list,
                       'msg': msg,
                      })

    rcpt_list = UserProfile_Recipient.objects.filter(user=request.user.id)
    return render(request, 'member/rcpt_list.html',
                  {'page_title': page_title,
                   'location': '배송지 목록',
                   'rcpt_list': rcpt_list,
                  })


# 배송지 추가
def view_add_recipient(request):
    if not request.user.is_authenticated():
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '에러',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })

    if request.method == 'POST':
        rcpt = UserProfile_Recipient(user=request.user)
        form = RecipientForm(request.POST, instance=rcpt)
        if form.is_valid():
            form.save()

            return redirect(view_recipient_list)
    else:
        form = RecipientForm()

    page_title = 'happyzehn : 배송지 추가'
    return render(request, 'form.html',
                  {'page_title': page_title,
                   'location': '배송지 추가',
                   'form': form,
                   })


# 1:1 상담 목록
def view_consult(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/?next=%s' % request.path)

    consult_list = Consult.objects.filter(user=request.user.id)
    answer_list = (Consult_Answer.objects
                   .filter(consult__in=consult_list))
    page_title = 'happyzehn : 1:1 상담 목록'

    return render(request, 'member/consult.html',
                  {'page_title':page_title,
                   'location': '1:1 상담 목록',
                   'consult_list': consult_list,
                   'answer_list': answer_list,
                  })


# 상담 작성
def view_write_consult(request):
    if not request.user.is_authenticated():
        return render(request, 'result.html',
                      {'type': 'error',
                       'location': '에러',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })

    if request.method == 'POST':
        consult = Consult(user=request.user)
        form = ConsultForm(request.POST, instance=consult)
        if form.is_valid():
            form.save()
            return redirect(view_consult)

    else:
        form = ConsultForm()

    page_title = 'happyzehn : 1:1 상담 작성'
    return render(request, 'form.html',
                  {'page_title': page_title,
                   'location': '1:1 상담 작성',
                   'form': form,
                   })


# 상담 답변(관리자)
def view_answer_consult(request, consult_id):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return render(request, 'result.html',
                      {'page_title': '요청 결과',
                       'location': '에러',
                       'type': 'error',
                       'msg': '잘못된 접근입니다.',
                       'redirect_to': 'default',
                       })

    if request.method == 'POST':
        answer = Consult_Answer(consult=consult_id)
        form = AnswerConsultForm(request.POST, instance=answer)
        if form.is_valid():
            form.save()
            return redirect('/')

    else:
        form = AnswerConsultForm()

    page_title = 'happyzehn : 1:1 상담 답변 작성'
    return render(request, 'form.html',
                  {'page_title': page_title,
                   'location': '1:1 상담 답변 작성',
                   'form': form,
                   })


# FAQ
def view_faq(request):
    page_title = 'happyzehn : FAQ'

    return render(request, 'member/faq.html',
                  {'page_title': page_title,
                   'location': '자주 묻는 질문',
                   })
