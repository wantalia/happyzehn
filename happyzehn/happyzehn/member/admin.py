# -*- coding: utf-8 -*-
'''
Created on 2013. 6. 13.

@author: KIm Wansu
'''
from django.contrib import admin
from happyzehn.member.models import UserProfile
from happyzehn.member.models import UserProfile_Recipient
from happyzehn.member.models import Consult
from happyzehn.member.models import Consult_Answer

admin.site.register(UserProfile)
admin.site.register(UserProfile_Recipient)
admin.site.register(Consult)
admin.site.register(Consult_Answer)
