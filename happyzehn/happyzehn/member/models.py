# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    user = models.ForeignKey(User, unique=True)
    address = models.CharField(max_length=200,
                               null=False, blank=False)
    phone = models.CharField(max_length=24,
                             null=False, blank=False)
    mileage = models.PositiveIntegerField(null=False)

    def __unicode__(self):
        return unicode(self.user)


class UserProfile_Recipient(models.Model):
    name = models.CharField(max_length=42, null=False, blank=False,
                            verbose_name='배송지 이름')
    user = models.ForeignKey(User)
    rcpt_name = models.CharField(max_length=42, null=False, blank=False,
                                 verbose_name='이름')
    rcpt_addr = models.CharField(max_length=200, null=False, blank=False,
                                 verbose_name='주소')
    rcpt_phone = models.CharField(max_length=24, null=False, blank=False,
                                  verbose_name='전화번호')

    def __unicode__(self):
        return unicode(self.user) + '-' + self.name


class Consult(models.Model):
    user = models.ForeignKey(User)
    title = models.CharField(max_length=42, null=False, blank=False,
                             verbose_name='제목')
    content = models.TextField(null=False, blank=False,
                               verbose_name='내용')
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.title


class Consult_Answer(models.Model):
    consult = models.OneToOneField(Consult)
    answer = models.TextField(max_length=2000, null=True, blank=False,
                              verbose_name='답변')
    answered = models.DateTimeField(auto_now_add=True, null=True)

    def __unicode__(self):
        return self.consult.title

