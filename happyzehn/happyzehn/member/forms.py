# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.forms import ModelForm
from happyzehn.member.models import UserProfile_Recipient
from happyzehn.member.models import Consult
from happyzehn.member.models import Consult_Answer

import re


# 로그인 폼
class LoginForm(forms.Form):
    username = forms.CharField(label="아이디", max_length=30)
    password = forms.CharField(label="암호", widget=forms.PasswordInput)

    def clean_username(self):
        username = self.cleaned_data['username']
        if username == '':
            raise forms.ValidationError('아이디는 반드시 입력해야 합니다.')

        if not re.search(r'^\w+$', username):
            raise forms.ValidationError('아이디는 알파벳, 숫자, 밀줄만 ' +
                                        '가능합니다.')
        return username

    def clean_password(self):
        password = self.cleaned_data['password']
        if password == '':
            raise forms.ValidationError('암호는 반드시 입력해야 합니다.')

        return password


# 회원 가입 폼
class SignUpForm(forms.Form):
    username = forms.CharField(label='아이디', max_length=30)
    lastname = forms.CharField(label='성', max_length=30)
    firstname = forms.CharField(label='이름', max_length=30)
    email = forms.EmailField(label='이메일',
                             help_text=('상품 주문 진행 정보를 받을 메일 ' +
                                        '주소입니다. 정확하게 입력하세요.'))
    password1 = forms.CharField(label='암호', widget=forms.PasswordInput)
    password2 = forms.CharField(label='암호 확인', widget=forms.PasswordInput)
    address = forms.CharField(label='주소',
                              help_text=('기본 배송지로 사용됩니다. ' +
                                         '정확하게 입력해주세요.'))
    phone = forms.CharField(label='전화번호')

    def clean_username(self):
        username = self.cleaned_data['username']
        if username == '':
            raise forms.ValidationError('아이디는 반드시 입력해야 합니다.')

        if not re.search(r'^\w+$', username):
            raise forms.ValidationError('아이디는 알파벳, 숫자, 밀줄만 ' +
                                        '가능합니다.')
        try:
            User.objects.get(username=username)
        except ObjectDoesNotExist:
            return username
        raise forms.ValidationError('이미 사용중인 아이디입니다.')

    def clean_lastname(self):
        lastname = self.cleaned_data['lastname']
        if lastname == '':
            raise forms.ValidationError('성은 반드시 입력해야 합니다.')
        return lastname

    def clean_firstname(self):
        firstname = self.cleaned_data['firstname']
        if firstname == '':
            raise forms.ValidationError('이름은 반드시 입력해야 합니다.')
        return firstname

    def clean_email(self):
        email = self.cleaned_data['email']
        if email == '':
            raise forms.ValidationError('이메일 주소는 반드시 입력해야 합니다.')
        return email

    def clean_password2(self):
        if 'password1' in self.cleaned_data:
            password1 = self.cleaned_data['password1']
            password2 = self.cleaned_data['password2']
            if password1 == password2:
                return password2
        raise forms.ValidationError('암호가 일치하지 않습니다.')

    def clean_address(self):
        address = self.cleaned_data['address']
        if address == '':
            raise forms.ValidationError('주소는 반드시 입력해야 합니다.')
        return address

    def clean_phone(self):
        phone = self.cleaned_data['phone']
        if phone == '':
            raise forms.ValidationError('전화번호는 반드시 입력해야 합니다.')
        return phone


# 암호 변경 폼
class PasswdForm(forms.Form):
    password1 = forms.CharField(label='암호', widget=forms.PasswordInput)
    password2 = forms.CharField(label='암호 확인', widget=forms.PasswordInput)

    def clean_password2(self):
        if 'password1' in self.cleaned_data:
            password1 = self.cleaned_data['password1']
            password2 = self.cleaned_data['password2']
            if password1 == password2:
                return password2
        raise forms.ValidationError('암호가 일치하지 않습니다.')


# 회원 정보 수정 폼
class UpdateForm(forms.Form):
    email = forms.EmailField(label='이메일',
                             help_text=('상품 주문 진행 정보를 받을 메일 ' +
                                        '주소입니다. 정확하게 입력하세요.'))
    address = forms.CharField(label='주소')
    phone = forms.CharField(label='전화번호')

    def clean_address(self):
        address = self.cleaned_data['address']
        if address == '':
            raise forms.ValidationError('주소는 반드시 입력해야 합니다.')
        return address

    def clean_phone(self):
        phone = self.cleaned_data['phone']
        if phone == '':
            raise forms.ValidationError('전화번호는 반드시 입력해야 합니다.')
        return phone


# 배송지 추가 폼
class RecipientForm(ModelForm):
    class Meta:
        model = UserProfile_Recipient
        exclude = ('user',)


# 1:1 상담 작성 폼
class ConsultForm(ModelForm):
    class Meta:
        model = Consult
        exclude = ('user', 'created',)


# 1:1 상담 답변 폼
class AnswerConsultForm(ModelForm):
    class Meta:
        model = Consult_Answer
        exclude = ('consult', 'answered',)
