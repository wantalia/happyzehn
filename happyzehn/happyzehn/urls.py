# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
                       # 처음 화면
                       url(r'^$', 'happyzehn.member.views.view_home'),
                       # 로그인
                       url(r'^login/$', 'happyzehn.member.views.view_login'),
                       # 로그아웃
                       url(r'^logout/$', 'happyzehn.member.views.view_logout'),


                       # 회원 가입
                       url(r'^signup/$',
                           'happyzehn.member.views.view_signup'),
                       # 암호 변경
                       url(r'^passwd/$',
                           'happyzehn.member.views.view_passwd'),
                       # 회원 정보 수정
                       url(r'^update/$',
                           'happyzehn.member.views.view_update'),


                       # 상품 목록
                       url(r'^category(?P<category>\d+)/(?P<page>\d+)$',
                           'happyzehn.goods.views.view_goodslist'),
                       # 상품 정보 보기
                       url(r'^goods/(?P<goods_id>\d+)$',
                           'happyzehn.goods.views.view_goods'),
                       # 상품평 작성
                       url(r'^review/(?P<goods_id>\d+)$',
                           'happyzehn.goods.views.view_write_review'),
                       # 상품 문의하기
                       url(r'^inquire/(?P<goods_id>\d+)$',
                           'happyzehn.goods.views.view_write_inquire'),
                       # 상품 문의 답변하기(관리자)
                       url(r'^inquire/answer/(?P<inquire_id>\d+)$',
                           'happyzehn.goods.views.view_answer_inquire'),


                       # 고객 센터
                       url(r'^cscenter/',
                           'happyzehn.member.views.view_cscenter'),
                       # 배송지 목록
                       url(r'^recipient/$',
                           'happyzehn.member.views.view_recipient_list'),
                       # 배송지 추가
                       url(r'^recipient/add/$',
                           'happyzehn.member.views.view_add_recipient'),
                       # 1:1 상담
                       url(r'^consult/$',
                           'happyzehn.member.views.view_consult'),
                       # 상담 작성
                       url(r'^consult/write/$',
                           'happyzehn.member.views.view_write_consult'),
                       # 상담 답변(관리자)
                       url(r'^consult/answer/(?P<consult_id>\d+)$',
                           'happyzehn.member.views.view_answer_consult'),
                       # FAQ
                       url(r'^faq/$', 'happyzehn.member.views.view_faq'),


                       # 장바구니에 담기
                       url(r'^add_to_cart/(?P<product_id>\d+)/(?P<quantity>\d+)/$',
                           'happyzehn.order.views.add_to_cart'),
                       # 장바구니에서 상품 삭제
                       url(r'^remove_from_cart/(?P<product_id>\d+)/$',
                           'happyzehn.order.views.remove_from_cart'),
                       # 장바구니의 상품 수량 변경
                       url(r'^update_cart_product_quantity/$',
                           'happyzehn.order.views.update_cart_product_quantity'),
                       # 장바구니 비우기
                       url(r'^clean_cart/$',
                           'happyzehn.order.views.clean_cart'),
                       # 장바구니
                       url(r'^cart/$', 'happyzehn.order.views.view_cart'),
                       # 배송지 선택
                       url(r'^choice_rcpt/$',
                           'happyzehn.order.views.view_choice_recipient'),
                       # 결제 수단 선택
                       url(r'^choice_payment/$',
                           'happyzehn.order.views.view_choice_payment'),
                       # 결제 정보 입력
                       url(r'^input_payment_info/$',
                           'happyzehn.order.views.view_input_payment_info'),
                        # 결제하기
                       url(r'^payment/$', 'happyzehn.order.views.view_payment'),
                       # 주문 현황
                       url(r'^order/$', 'happyzehn.order.views.view_order'),
                       # 주문 중단
                       url(r'^order/stop/$', 'happyzehn.order.views.view_stop_order'),
                       # 주문 취소/환불 신청
                       url(r'^order/cancel/$',
                           'happyzehn.order.views.view_cancel'),
                       # 주문 결정
                       url(r'^order/confirm/$',
                           'happyzehn.order.views.view_confirm_order'),


                       # 관리자 페이지
                       url(r'^admin/', include(admin.site.urls)),
                       ) + static(settings.MEDIA_URL,\
                                  document_root = settings.MEDIA_ROOT)
